# your custom models go here
from django.db import models
from oscar.apps.partner.abstract_models import AbstractPartner
class Partner(AbstractPartner):
	# Vendor personal detail fields
	first_name=models.CharField(max_length=30, blank=True,null=True)
	last_name=models.CharField(max_length=30, blank=True,null=True)
	email=models.EmailField(max_length=100,blank=True,null=True)
	address=models.TextField(blank=True,null=True)
	pincode=models.CharField(max_length=20, blank=True,null=True)
	state=models.CharField(max_length=50, blank=True,null=True)
	partner_logo = models.ImageField(upload_to='partner_logo', blank=True, null=True)
	# Vendor account detail fields
	account_name=models.CharField(max_length=255, blank=True,null=True)
	account_number=models.IntegerField(blank=True,null=True)
	ifsccode=models.CharField(max_length=50,blank=True,null=True)
	bank_name=models.CharField(max_length=100, blank=True,null=True)
	bank_address=models.TextField(blank=True,null=True)

	admin_commsion=models.CharField(max_length=50, blank=True,null=True)
	gross_sales=models.CharField(max_length=50, blank=True,null=True)
	total_eraning=models.FloatField(max_length=50, blank=True,null=True)
	current_balance=models.FloatField(max_length=50, blank=True,null=True)
from customoscar.partners.models import *
from oscar.apps.partner.models import *