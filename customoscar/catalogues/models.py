# your custom models go here
from django.db import models
from oscar.apps.catalogue.abstract_models import AbstractProduct,AbstractCategory
class Product(AbstractProduct):
    video = models.FileField(upload_to='product-videos',blank=True,null=True)
    thumbnail = models.ImageField(upload_to='product-thumbnail', blank=True, null=True)

from customoscar.catalogues.models import *
from oscar.apps.catalogue.models import *