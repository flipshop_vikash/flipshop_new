import collections
from django.conf import settings
from oscarapi.views.root import ADMIN_APIS, PUBLIC_APIS
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse
 
def PUBLIC_APIS2(r, f):
    x = PUBLIC_APIS(r,f)
    return x + [("Product-List", reverse("product-list", request= r, format= f)),
    ]


@api_view(("GET",))
def api_root(request, format=None):  # pylint: disable=redefined-builtin
    """
    GET:
    Display all available urls.

    Since some urls have specific permissions, you might not be able to access
    them all.
    """

    apis = PUBLIC_APIS2(request, format)
    
    if not getattr(settings, "OSCARAPI_BLOCK_ADMIN_API_ACCESS", True) and request.user.is_staff:
        apis += [
            ("admin", collections.OrderedDict(ADMIN_APIS(request, format))),
            ]

    return Response(collections.OrderedDict(apis))

    

