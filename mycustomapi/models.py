# from __future__ import unicode_literals
#
from phonenumber_field.serializerfields import PhoneNumberField

from django.contrib.auth.models import AbstractBaseUser
from django.db import models
from django.utils.translation import ugettext_lazy as _

#
class User(AbstractBaseUser):
    phone_number = models.CharField(max_length=250)

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')






# Create your models here.""
