from .serializers import ProductLinkSerializer,ShowImageSerializer,categorySerializer,cart
import json
from rest_framework.response import Response
from customoscar.catalogues.models import Product
from oscar.core.loading import get_class, get_model
ProductImage = get_model('catalogue', 'ProductImage')
ProductCategory = get_model('catalogue', 'ProductCategory')
productbasket=get_model('basket','Line')
from .viewset import ResponseModelViewSet


class ShowImageList(ResponseModelViewSet):
    queryset = ProductImage.objects.all()
    serializer_class = ShowImageSerializer

    def post(self, request, *args, **kwargs):
        file = request.data['original']
        product = request.data['product']
        caption = request.data['caption']
        display_order = request.data['display_order']
        p = Product.objects.get(pk=product)
        ProductImage.objects.create(product=p, original=file, caption=caption, display_order=display_order)
        return Response(json.dumps({'message': "Uploaded"}))

class ShowImageDetail(ResponseModelViewSet):
    queryset = ProductImage.objects.all()
    serializer_class = ShowImageSerializer


class ProductList(ResponseModelViewSet):
    serializer_class = ProductLinkSerializer
    model=Product
    def get_queryset(self):
        return Product.objects.all()

class Categories(ResponseModelViewSet):
    queryset = ProductCategory.objects.all()
    serializer_class = categorySerializer

class like(ResponseModelViewSet):
    a=ResponseModelViewSet

class basket(ResponseModelViewSet):
    queryset = productbasket.objects.all()
    serializer_class = cart
