from django.conf.urls import include, url
from .root import api_root
from .views import ProductList
from .view import (
    change_password,
    login,
    logout,
    profile,
    register,
    register_email,
    reset_password,
    send_reset_password_link,
    verify_email,
    verify_registration
)
from .views import ShowImageList,ShowImageDetail,Categories,like,basket


urlpatterns = [
url('^register/$', register, name='register'),
url('^verify-registration/$', verify_registration, name='verify-registration'),
url('^send-reset-password-link/$', send_reset_password_link, name='send-reset-password-link'),
url('^reset-password/$', reset_password, name='reset-password'),
url('^login/$', login, name='login'),
url('^logout/$', logout, name='logout'),
url('^profile/$', profile, name='profile'),
url('^change-password/$', change_password, name='change-password'),
url('^register-email/$', register_email, name='register-emai    l'),
url('^verify-email/$', verify_email, name='verify-email'),
url (r'^root/$',api_root, name ="api-root"),
url(r'^products',ProductList.as_view({'get': 'retrieve'}), name="product-list"),
url(r'^show_image/$', ShowImageList.as_view({'get': 'retrieve'}), name='ShowImage'),
url(r'^show_image/(?P<pk>[\w-]+)/$', ShowImageDetail.as_view({'get': 'retrieve'}), name='image_detail'),
url(r'^categories/', Categories.as_view({'get': 'retrieve'}), name= 'categories'),
url(r'^popular/',ProductList.as_view({'get': 'retrieve'}),name='popular'),
url(r'^wishlist',ProductList.as_view({'get': 'retrieve'}), name='wishlist'),
url(r'^sponser/popular/products', ProductList.as_view({'get': 'retrieve'}), name='wishlist'),
url(r'^like',like.as_view({'get': 'retrieve'}), name='like'),
url(r'^dislike',like.as_view({'get': 'retrieve'}), name='dislike'),
url(r'^basket',basket.as_view({'get': 'retrieve'}), name='basket'),
# url(r'^checkout')
url(r'', include('oscarapi.urls')),
] 